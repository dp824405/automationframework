package com.bank;

public abstract class Bank {

	private static final String NAME = "HDFC"; // CONSTANT

	public void createAccount(String document) {
		if (verifyDocument(document)) {
			System.out.println("Account Created");
		} else {
			System.out.println("Nope!!");
		}
	}

	public  abstract boolean verifyDocument(String document);
}
