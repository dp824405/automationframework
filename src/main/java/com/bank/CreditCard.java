package com.bank;

public interface CreditCard {
	public static final int INTEREST = 20;
	
	public abstract void applyForLoan() ;
}
