package com.bank;

public interface ILoan {
	public static final int INTEREST = 9;
	
	public abstract void applyForLoan() ;
}
