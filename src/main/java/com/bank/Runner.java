package com.bank;

public class Runner {
public static void main(String[] args) {
	
	Bank neeta = new ABCBranch();
	neeta.createAccount("passport");
	((BandraBranch)neeta).demo();
	
	DelhiBranch virag = new DelhiBranch();
	virag.createAccount("DL");
	
	ILoan loan = new ABCBranch();
	loan.applyForLoan();
}
}
