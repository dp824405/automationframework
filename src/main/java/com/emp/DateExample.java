package com.emp;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateExample {
public static void main(String[] args) {
	
	
	
	Date date = new Date();
	System.out.println(date.toString());
	
	SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY HH-mm");
	String formatedDate= sdf.format(date);
	
	System.out.println(formatedDate);
}
}
