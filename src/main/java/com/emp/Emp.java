package com.emp;

public abstract class Emp {
	private int empId;
	private String name;
	private String projectName;
	private String skillset;
	int x;
	public Emp(int empId, String name, String projectName, String skillset) {
		
		this.empId = empId;
		this.name = name;
		this.projectName = projectName;
		this.skillset = skillset;
	}
	@Override
	public String toString() {
		return "Emp [empId=" + empId + ", name=" + name + ", projectName=" + projectName + ", skillset=" + skillset
				+ "]";
	}
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getSkillset() {
		return skillset;
	}
	public void setSkillset(String skillset) {
		this.skillset = skillset;
	}
	
	
}
