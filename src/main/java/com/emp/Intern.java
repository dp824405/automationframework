package com.emp;

public final class Intern extends Dev {

	public Intern(int empId, String name, String projectName, String skillset) {
		super(empId, name, projectName, skillset);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Intern [getEmpId()=" + getEmpId() + ", getName()=" + getName() + ", getProjectName()="
				+ getProjectName() + ", getSkillset()=" + getSkillset() + "]";
	}

	public void work() {
		System.out.println("Intern");
	}

	public void work(int jobid) {

	}

	public void work(String task) {

	}

}
