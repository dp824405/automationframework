package com.read.csv.example;

public class Runner_Singleton {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Demo d1 = Demo.getInstance();
		System.out.println(d1);

		Demo d2 = Demo.getInstance();
		System.out.println(d2);

		Demo d3 = Demo.getInstance();
		System.out.println(d3);
	}

}
