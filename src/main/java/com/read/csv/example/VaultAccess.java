package com.read.csv.example;

import com.bettercloud.vault.*;
import com.bettercloud.vault.response.*;

public class VaultAccess {
	public static void main(String[] args) throws VaultException {
		String vaultToken = "hvs.y0WWIiwRtLlAIF2kaXJJUxp7";

		String vaultAddress = "http://localhost:8200"; // Replace with your Vault server address

		VaultConfig vaultConfig = new VaultConfig().address(vaultAddress).token(vaultToken).build();
		Vault vault = new Vault(vaultConfig);

		LogicalResponse response = vault.logical().read("myapp/database"); // Replace with your Vault path

		if (response != null && response.getData() != null) {
			String dbURL = response.getData().get("DBURL");
			String dbUsername = response.getData().get("USERNAME");
			String dbPassword = response.getData().get("PASSWORD");
			System.out.println(dbURL);
			System.out.println(dbUsername);
			System.out.println(dbPassword);


		} else {
			System.out.println("Database credentials not found in Vault");
		}
	}
}
