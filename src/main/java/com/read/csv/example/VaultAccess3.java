package com.read.csv.example;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;



public class VaultAccess3 {
	public static void main(String[] args) throws ClientProtocolException, IOException {
		String vaultAddress = "http://localhost:8200"; // Replace with your Vault server address
		String vaultToken = "hvs.Kaz0AVO7Ahqu2GsuHIJpHMGt"; // Replace with your authentication token
		String databaseRole = "myrole"; // Replace with your Vault database role name
		String databasePath = "database/creds/" + databaseRole;

		HttpClient httpClient = HttpClients.createDefault();

		// Create an HTTP GET request to fetch dynamic credentials
		HttpGet httpGet = new HttpGet(vaultAddress + "/v1/" + databasePath);
		httpGet.addHeader("X-Vault-Token", vaultToken);

		// Execute the request
		HttpResponse response = httpClient.execute(httpGet);
		HttpEntity entity = response.getEntity();

		if (entity != null) {
			String responseBody = EntityUtils.toString(entity);
			JSONObject jsonResponse = new JSONObject(responseBody);

			if (jsonResponse.has("data")) {
				JSONObject data = jsonResponse.getJSONObject("data");
				String username = data.getString("username");
				String password = data.getString("password");

				System.out.println("Generated username: " + username);
				System.out.println("Generated password: " + password);
			} else {
				System.out.println("No data found in the response.");
			}
		} else {
			System.out.println("Empty response from Vault.");
		}
	}

}
