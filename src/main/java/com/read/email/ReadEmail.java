package com.read.email;

import javax.mail.*;
import javax.mail.internet.*;

import java.io.IOException;
import java.util.Properties;

public class ReadEmail {
	public static void main(String[] args) {
		final String username = "testautomationacademy33@gmail.com";
		final String password = "nijq rnsf kkut gavy"; //hxbs kirl ioxg erog

		Properties prop = new Properties();
		prop.put("mail.store.protocol", "imaps"); // Changed protocol to "imaps"
		prop.put("mail.imaps.host", "imap.gmail.com");
		prop.put("mail.imaps.port", "993");
		prop.put("mail.imaps.auth", "true");

		Session session = Session.getInstance(prop, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {
			// Connect to the Gmail server
			Store store = session.getStore("imaps"); // Changed to "imaps"
			store.connect("imap.gmail.com", username, password);

			// Get the inbox folder
			Folder inbox = store.getFolder("INBOX");
			inbox.open(Folder.READ_ONLY);

			// Get messages in the inbox
			Message[] messages = inbox.getMessages();

			// Display information about each message
			Message message = messages[messages.length - 1]; // 0 n-1
			System.out.println("Subject: " + message.getSubject());
			System.out.println("From: " + message.getFrom()[0]);
			System.out.println("Sent Date: " + message.getSentDate());
			System.out.println("Received Date: " + message.getReceivedDate());
			System.out.println("------------------------------------");
			Object content = message.getContent();

			if (content instanceof MimeMultipart) {
				String data = handleMultipart((MimeMultipart) content);
				getOTP(data);
			} else {
				System.out.println("Content: " + content);
			}

			System.out.println("------------------------------------");

			// Close the connection
			inbox.close(false);
			store.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static String handleMultipart(MimeMultipart multipart) throws MessagingException, IOException {
		String messagebody = null;

		BodyPart bodyPart = multipart.getBodyPart(0);
		Object partContent = bodyPart.getContent();

		if (partContent instanceof String) {
			System.out.println("Part Content: " + partContent);
			messagebody = partContent + "";
		}

		return messagebody;
	}

	public static void getOTP(String data) {
		System.out.println(data.substring(12,17));
	}

}