package com.ui.examples;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public abstract class BrowserUtil2 {
	private WebDriver wd;

	public WebDriver getWd() {
		return wd;
	}

	public BrowserUtil2(WebDriver wd) {
		super();
		this.wd = wd;
	}

	public void goToWebSite(String url) {
		wd.get(url);

	}

	public void viewInFullScreen() {
		wd.manage().window().maximize();
	}

	public void enterText(By locator, String textToEnter) {
		sleepFor(4);
		WebElement element = wd.findElement(locator);
		element.clear();
		element.sendKeys(textToEnter);
	}

	public void clickOn(By locator) {
		sleepFor(4);
		WebElement element = wd.findElement(locator);
		element.click();
	}

	public void sleepFor(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
