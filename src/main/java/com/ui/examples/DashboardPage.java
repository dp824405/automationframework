package com.ui.examples;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class DashboardPage extends BrowserUtil {

	private static final By CREATE_JOB_LINK_LOCATOR = By.xpath("//span[contains(text(),\"Create Job\")]/../../..");

	private WebDriver wd;

	public DashboardPage(WebDriver wd) {
		super(wd);
		this.wd = wd;

	}

	public CreateJobPage goToCreateJobPage() {
		clickOn(CREATE_JOB_LINK_LOCATOR);
		CreateJobPage createJobPage = new CreateJobPage(wd);
		return new CreateJobPage(wd);
	}
}
