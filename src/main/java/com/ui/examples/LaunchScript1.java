package com.ui.examples;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LaunchScript1 {
	public static void main(String[] args) {

		
		WebDriver wd = new ChromeDriver();
		wd.get("http://phoenix.testautomationacademy.in/sign-in");

		wd.manage().window().maximize();
		
		By userNameTextBoxLocator=By.id("username");
		
		WebElement userNameTextBoxWebElement=wd.findElement(userNameTextBoxLocator);
		userNameTextBoxWebElement.clear();
		userNameTextBoxWebElement.sendKeys("iamsup");
		
		By passwordTextBoxLocator=By.id("password");
		
		WebElement passwordTextBoxWebElement=wd.findElement(passwordTextBoxLocator);
		passwordTextBoxWebElement.clear();
		passwordTextBoxWebElement.sendKeys("password");
		
		
		
		
		
		
		By signInButtonLocator=	By.xpath("//span[contains(text(),\"Sign in\")]/../..");
		WebElement signInButtonElement=wd.findElement(signInButtonLocator);
		signInButtonElement.click();
		
		
		
		
		
	}
}
