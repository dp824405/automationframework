package com.ui.examples;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class LaunchScript4 {
	public static void main(String[] args) {

		
		WebDriver wd = new ChromeDriver();
		LoginPage page = new LoginPage(wd);
		
		page.doLogin("iamfd", "password").goToCreateJobPage().createJob();


	}
}
