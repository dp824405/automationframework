package com.ui.examples;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class LaunchScript5 {
	public static void main(String[] args) throws IOException {

		
		WebDriver wd = new ChromeDriver();
		LoginPage page = new LoginPage(wd);
		
		page.doLogin("iamfd", "password").goToCreateJobPage().createJob();
		
		
		//TAKE SCREENSHOT in WD
		
		TakesScreenshot takesScreenshot = (TakesScreenshot) wd; // TC
		File screenShotData=takesScreenshot.getScreenshotAs(OutputType.FILE); //1010111 111111
		
		File myFile = new File(System.getProperty("user.dir")+"//abc.png");
		myFile.createNewFile();
		FileUtils.copyFile(screenShotData, myFile); //APACHE COMMON
		


	}
}
