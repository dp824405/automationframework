package com.ui.scenarios;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class HandlingJSAlerts {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		WebDriver wd = new ChromeDriver();
		wd.get("http://139.59.91.96:5001/selenium-workbook/popups.html");

		By alertButtonLocator = By.id("alert");
		WebElement alertButton = wd.findElement(alertButtonLocator);
		alertButton.click();

		// How to handle Alerts in Wd
		Thread.sleep(2000);
		Alert myAlert = wd.switchTo().alert();
		String data = myAlert.getText();
		System.out.println(data);

		// Click on the OK button in Alert POP Up
		Thread.sleep(2000);

		myAlert.accept();
	}

}
