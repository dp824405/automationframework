package com.ui.scenarios;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class HandlingModal {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		WebDriver wd = new ChromeDriver();
		wd.get("http://139.59.91.96:5001/selenium-workbook/popups.html");

		By modalButtonLocator = By.linkText("Modal");
		WebElement modalButton = wd.findElement(modalButtonLocator);
		modalButton.click();

		By modalTextBoxLocator = By.xpath("//div[@class=\"modal-body\"]/p/input");
		WebElement modalTextBox = wd.findElement(modalTextBoxLocator);
		modalTextBox.sendKeys("Hello I am here!!");

		By okayButtonLocator = By.xpath("//button[contains(text(),\"Ok\")]");
		WebElement okayButton = wd.findElement(okayButtonLocator);
		okayButton.click();

		By cancelButtonLocator = By.xpath("//button[contains(text(),\"Cancel\")]");
		WebElement cancelButton = wd.findElement(cancelButtonLocator);
		cancelButton.click();

	}

}
