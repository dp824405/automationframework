package com.ui.scenarios;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class HandlingOfFrame {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		WebDriver wd = new ChromeDriver();
		wd.get("http://139.59.91.96:5001/selenium-workbook/wyswyg-editor.html");

		By headingLocator = By.tagName("h1");
		
		WebElement headingElement=wd.findElement(headingLocator);
		System.out.println(headingElement.getText());
		
		wd.switchTo().frame("editor_ifr"); // switch to frame
		
		By bodyLocator = By.id("tinymce");
		WebElement body=wd.findElement(bodyLocator);
		body.sendKeys("abc");
		wd.switchTo().parentFrame(); //exit from the child frame and it will switch to parent page!
		System.out.println(headingElement.getText());
	}
	

}
