package com.ui.scenarios;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ModalPage extends BrowserUtil {

	By modalTextBoxLocator = By.xpath("//div[@class=\"modal-body\"]/p/input");
	By okayButtonLocator = By.xpath("//button[contains(text(),\"Ok\")]");
	By cancelButtonLocator = By.xpath("//button[contains(text(),\"Cancel\")]");
	private WebDriver wd;

	public ModalPage(WebDriver wd) {
		super(wd);
		this.wd = wd;
	}
	
	
	public PromptPage enterText(String text) {
		enterText(modalTextBoxLocator, text);
		sleepFor(5);
		clickOn(cancelButtonLocator);
		return new PromptPage(wd);
	}
}
