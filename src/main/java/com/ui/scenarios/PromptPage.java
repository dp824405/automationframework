package com.ui.scenarios;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PromptPage extends BrowserUtil {

	private static final By modalButtonLocator = By.linkText("Modal");
	private static final By alertButtonLocator = By.id("alert");
	private WebDriver wd;

	public PromptPage(WebDriver wd) {
		super(wd);
		this.wd = wd;
	}

	public ModalPage clickOnModal() {
		clickOn(modalButtonLocator);
		return new ModalPage(wd);
	}
}
