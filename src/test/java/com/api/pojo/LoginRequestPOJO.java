package com.api.pojo;

public final class LoginRequestPOJO {
	private String username;
	private String password;
	private static int x;
	
	static {
		System.out.println("Inside Static Block 1 ");
		x =10;
	}
	
	static {
		System.out.println("Inside Static Block2");
	}
	static {
		System.out.println("Inside Static Block3");
	}

	static {
		System.out.println("Inside Static Block4");
	}
	static {
		System.out.println("Inside Static Block 5");
	}
	
	
	public LoginRequestPOJO(String username, String password) {
		super();
		System.out.println("Constructor of Login POJO");
		this.username = username;
		this.password = password;
	}
	public LoginRequestPOJO(char username, char password) {
		super();
		System.out.println("Constructor of Login POJO");
		
	}
	
	public LoginRequestPOJO() {
		
		this("a","v");
		System.out.println("Constructor of Login POJO");
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "LoginRequestPOJO [username=" + username + ", password=" + password + "]";
	}

}
