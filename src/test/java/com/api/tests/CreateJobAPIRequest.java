package com.api.tests;

import static io.restassured.RestAssured.*;

import static org.hamcrest.Matchers.*;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.utility.TestUtility;

import static com.utility.TestUtility.*;

import io.restassured.http.Header;

@Listeners(com.listeners.MyTestListener.class)
public final class CreateJobAPIRequest {
	private Header h1 ;
	private	Header h2;
	private	int jobNumber;
	
	
	static {
		baseURI = "http://139.59.91.96:9000/v1";
	}
	
	@BeforeMethod(description = "Intializing the headers", alwaysRun = true)
	public void setup() {
		h1 = new Header("Content-type", "application/json");
		 h2 = new Header("Authorization", TestUtility.getTokenFor("fd"));
		
	}
	

	@Test(description = "test create job api requests generates job number", groups = {"sanity", "smoke",})
	
	public void createjobTest() {
		// TODO Auto-generated method stub
		
		 
 TestUtility.jobId=		given()
			.header(h1)
		.and()
			.header(h2)
		.and()
			.body(getJsonObject( getCreatJobPOJO()))
			.log().all()
		.when()
			.post("/job/create")
		.then()
			.log().all()
		.assertThat()
			.statusCode(200)
		.and()
			.body("message", equalTo("Job created successfully. "))
		.and()
			.extract().jsonPath().getInt("data.id");
	System.out.println(jobNumber);		
	
	}
	
	
	@Test(description = "Verify the Details from the d", groups = {"sanity", "smoke"}, dependsOnMethods = {"createjobTest"})

	public void validateEntriesInDB() {
		
	}

}
