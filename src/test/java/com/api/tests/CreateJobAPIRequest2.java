package com.api.tests;

import static io.restassured.RestAssured.*;

import static org.hamcrest.Matchers.*;

import java.sql.SQLException;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.api.pojo.CreateJOBResponse;
import com.dao.TR_HOB_HEAD_DAO;
import com.db.pojo.HOB_HEAD_POJO;
import com.utility.TestUtility;

import static com.utility.TestUtility.*;

import io.restassured.http.Header;
import io.restassured.response.Response;

public final class CreateJobAPIRequest2 {
	private Header h1;
	private Header h2;
	private int jobNumber;
	CreateJOBResponse responsePOJO;
	static {
		baseURI = "http://139.59.91.96:9000/v1";
	}

	@BeforeMethod(description = "Intializing the headers")
	public void setup() {
		h1 = new Header("Content-type", "application/json");
		h2 = new Header("Authorization", TestUtility.getTokenFor("fd"));

	}

	@Test(description = "test create job api requests generates job number", groups = { "sanity", "smoke"})

	public void createjobTest() {
		// TODO Auto-generated method stub

		Response response = given().header(h1).and().header(h2).and().body(getJsonObject(getCreatJobPOJO())).log().all()
				.when().post("/job/create");

		System.out.println(response.asString());
		responsePOJO = TestUtility.convertJSONtoCreateJOBResponsePOJO(response.asString());
		System.out.println(responsePOJO);
	}

	@Test(description = "Verify the Details from the d", groups = { "sanity", "smoke" }, dependsOnMethods = {
			"createjobTest" })

	public void validateEntriesInDB() throws SQLException {
		TR_HOB_HEAD_DAO dao = new TR_HOB_HEAD_DAO();
		HOB_HEAD_POJO dataFromDB = dao.getJobDetails(responsePOJO.getData().getId());
		Assert.assertEquals(dataFromDB.getId(), responsePOJO.getData().getId());
		Assert.assertEquals(dataFromDB.getMst_oem_id(), responsePOJO.getData().getMst_oem_id());
		Assert.assertEquals(dataFromDB.getTr_customer_id(), responsePOJO.getData().getTr_customer_id());

	}

}
