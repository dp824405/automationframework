package com.api.tests;

import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public final class LoginAPIRequests {
	public static void main(String[] args) {
		RestAssured.baseURI = "http://139.59.91.96:9000/v1";

		Header myHeader = new Header("Content-Type", "application/json");

		// Make a request
		RequestSpecification request = RestAssured.given();
		request.header(myHeader);

		request.body("{\r\n" + "    \"username\": \"iamfd\",\r\n" + "    \"password\": \"password\"\r\n" + "}");
		Response response = request.post("/login");
		System.out.println("Response Body " + response.asPrettyString());
		System.out.println("SC " + response.statusCode());
		System.out.println("time" + response.time());
	}
}
