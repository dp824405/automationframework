package com.api.tests;

import com.api.pojo.LoginRequestPOJO;
import com.utility.TestUtility;

import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class LoginAPIRequests2 {
	static {
		RestAssured.baseURI = "http://139.59.91.96:9000/v1";

	}

	public static void main(String[] args) {

		Header myHeader = new Header("Content-Type", "application/json");

		// Make a request
		RequestSpecification request = RestAssured.given();
		request.header(myHeader);
		LoginRequestPOJO loginPOJO = new LoginRequestPOJO("iamfd", "password");
		request.body(TestUtility.getJsonObject(loginPOJO));
		Response response = request.post("/login");
		System.out.println("Response Body " + response.asPrettyString());
		System.out.println("SC " + response.statusCode());
		System.out.println("time" + response.time());
	}
}
