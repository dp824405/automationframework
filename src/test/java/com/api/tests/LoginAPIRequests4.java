package com.api.tests;

import com.api.pojo.LoginRequestPOJO;
import static com.utility.TestUtility.*;

import static io.restassured.RestAssured.*;

import static org.hamcrest.Matchers.*;

import java.io.File;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import io.restassured.http.Header;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

@Listeners(com.listeners.MyTestListener.class)
public class LoginAPIRequests4 {
	static {
		baseURI = "http://139.59.91.96:9000/v1";

	}
	
	File jsonSchemaFile = new File(System.getProperty("user.dir")+"//src//test//resources//responseSchemas//loginResponseSchema.json"); 
	
	@Test(description = "test login api request", groups = {"sanity"},
			dataProviderClass = com.dataproviders.LoginDataProvider.class,
			dataProvider = "LoginDATA",
			retryAnalyzer =  com.listeners.ReRunTest.class
			
			
			)

	public  void loginAPITest(String username, String password) {

		Header myHeader = new Header("Content-Type", "application/json");
		LoginRequestPOJO loginPOJO = new LoginRequestPOJO(username, password);

String token=given()
				.when()
					.header(myHeader)
				.and()
					.body(getJsonObject(loginPOJO))
				.and()
					.log().all()
					.post("/login")
				.then()
					.log().all()
					.assertThat()
					.statusCode(200)
					.and()
					.time(lessThan(1500L))
				.and()
					.body(JsonSchemaValidator.matchesJsonSchema(jsonSchemaFile))
				.and()
					.body("message", equalTo("Success"))
				.and()
					.extract().jsonPath().getString("data.token");
	System.out.println("-----------------------------------------");	
	System.out.println(token);
	}
}
