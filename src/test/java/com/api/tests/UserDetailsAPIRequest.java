package com.api.tests;

import org.testng.annotations.Test;

import com.utility.TestUtility;

import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class UserDetailsAPIRequest {
	@Test(description = "test userDetails api requests ", groups = { "smoke", "e2e" })

	public void userDetailsAPIRequest() {
		// TODO Auto-generated method stub
		
		RestAssured.baseURI = "http://139.59.91.96:9000/v1";

		Header header = new Header("Authorization",
				TestUtility.getTokenFor("fd"));

		RequestSpecification request = RestAssured.given();
		request.header(header);
		Response response = request.get("userdetails");
		System.out.println(response.asPrettyString());
	}

}
