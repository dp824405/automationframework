package com.ui.filters;

import java.util.function.Predicate;

import com.ui.pojo.DashboardTablePOJO;

public class ImeiFilter implements Predicate<DashboardTablePOJO> {
	public String imei;

	public ImeiFilter(String imei) {
		super();
		this.imei = imei;
	}

	@Override
	public boolean test(DashboardTablePOJO t) {
		// TODO Auto-generated method stub
		if (imei.equals(t.getImei())) {
			return true;
		} else {
			return false;
		}
	}

}
