package com.ui.filters;

import java.util.function.Predicate;

import com.ui.pojo.DashboardTablePOJO;

public class OemFillter implements Predicate<DashboardTablePOJO> {

	public String oem;

	public OemFillter(String oem) {
		super();
		this.oem = oem;
	}

	@Override
	public boolean test(DashboardTablePOJO t) {
		// TODO Auto-generated method stub
		if (oem.equalsIgnoreCase(t.getOEM())) {
			return true;
		} else {
			return false;
		}
	}

}
