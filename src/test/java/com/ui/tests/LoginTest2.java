package com.ui.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTest2 {
	@Test()
	public void loginTest() {
		WebDriver wd = new ChromeDriver();
		wd.get("http://testautomationacademy.in/sign-in");
		
		By userNameTextBoxLocator=By.id("username");
		WebElement userNameTextBoxWebElement=wd.findElement(userNameTextBoxLocator);
		userNameTextBoxWebElement.sendKeys("Admin");
		
		By passwordTextBoxLocator=By.id("password");
		WebElement passwordTextBoxWebElement=wd.findElement(passwordTextBoxLocator);
		passwordTextBoxWebElement.sendKeys("password");		
		
		By signInButtonLocator=	By.xpath("//button");
		WebElement signInButtonElement=wd.findElement(signInButtonLocator);
		signInButtonElement.click();
	
		
	}
}
