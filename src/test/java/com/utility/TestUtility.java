package com.utility;

import static com.utility.TestUtility.getJsonObject;
import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeMultipart;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.api.pojo.CreateJOBResponse;
import com.api.pojo.CreateJobPOJO;
import com.api.pojo.Customer;
import com.api.pojo.CustomerAddress;
import com.api.pojo.CustomerProduct;
import com.api.pojo.LoginRequestPOJO;
import com.api.pojo.Problem;
import com.github.javafaker.Faker;
import com.google.gson.Gson;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import com.ui.filters.ImeiFilter;
import com.ui.pojo.DashboardTablePOJO;

import io.restassured.http.Header;

/**
 * @author Admin
 *
 */

public abstract class TestUtility {
	
	public static String readProperty(String env, String key) {
		File file = new File(System.getProperty("user.dir") + "//config//" + env + ".properties");
		FileReader reader = null;
		Properties properties = new Properties();
		try {
			reader = new FileReader(file);
			properties.load(reader);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return properties.getProperty(key);
	}

	public static int jobId;
	
	/**
	 * Utility Method for reading the excel file to pass to dataprovider method
	 * @return String [][]
	 * @throws IOException
	 */
	public static String[][] readExcelFile() throws IOException {

		XSSFWorkbook myWorkbook = new XSSFWorkbook(System.getProperty("user.dir") + "//testData//LoginExcel.xlsx");
		XSSFSheet mySheet = myWorkbook.getSheetAt(0);
		int lastIndexOfRow = mySheet.getLastRowNum();
		XSSFRow myHeader = mySheet.getRow(0);
		int totalNumberOfCols = myHeader.getLastCellNum();
		System.out.println("Last Index of Row is" + lastIndexOfRow);
		System.out.println("total Number of Cols is " + totalNumberOfCols);

		String myData[][] = new String[lastIndexOfRow][totalNumberOfCols];
		XSSFRow myRow;
		XSSFCell myCell;
		for (int rowIndex = 1; rowIndex <= lastIndexOfRow; rowIndex++) {
			for (int colIndex = 0; colIndex < totalNumberOfCols; colIndex++) {
				myRow = mySheet.getRow(rowIndex);
				myCell = myRow.getCell(colIndex);
				myData[rowIndex - 1][colIndex] = myCell.getStringCellValue(); // You have read excel and stored it in a
																				// 2D
			}
			System.out.println("");
		}

		return myData;
	}

	
	/**
	 * It will be used in the API request to pass the JSON body
	 * @param pojo
	 * @return JSON representation of the POJO in string format. 
	 * 
	 */
	public static String getJsonObject(Object pojo) {
		Gson gson = new Gson();
		String jsonData = gson.toJson(pojo);
		return jsonData;
	}

	public static CreateJOBResponse convertJSONtoCreateJOBResponsePOJO(String jsonData) {
		Gson gson = new Gson();
		CreateJOBResponse pojo = gson.fromJson(jsonData, CreateJOBResponse.class);
		return pojo;
	}

	public static Iterator<String[]> readCSVFile(String fileName) {

		File csvFile = new File(System.getProperty("user.dir") + "//testData//" + fileName);
		FileReader fileReader = null;
		CSVReader csvReader;
		List<String[]> dataList = null;
		try {
			fileReader = new FileReader(csvFile);
			csvReader = new CSVReader(fileReader);
			dataList = csvReader.readAll();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CsvException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Iterator<String[]> dataIterator = dataList.iterator();

		dataIterator.next(); // oth retrived but i will not store
		return dataIterator;

	}

	public static CreateJobPOJO getCreatJobPOJO() {
		Faker faker = new Faker();
		String fName = faker.name().name();
		System.out.println(fName);
		String emailAddress = faker.internet().emailAddress();
		String contactNumber = faker.numerify("98########");
		Customer customer = new Customer(fName, "N", contactNumber, null, emailAddress, null);
		CustomerAddress address = new CustomerAddress("101", "ABC Apt", "XYZ street", "PQR", "area", "410042", "India",
				"maharashtra");

		String imei = faker.numerify("124############");
		CustomerProduct product = new CustomerProduct("2023-06-10T18:30:00.000Z", imei, imei, imei,
				"2023-06-10T18:30:00.000Z", 1, 1);
		Problem[] problems = new Problem[2];
		problems[0] = new Problem(1, "test");
		problems[1] = new Problem(2, "test 233245");

		CreateJobPOJO createJobPOJO = new CreateJobPOJO(0, 2, 1, 1, customer, address, product, problems);
		return createJobPOJO;
	}

	public static String getTokenFor(String role) {
		LoginRequestPOJO loginPojo = null;
		if (role.equalsIgnoreCase("fd")) {
			loginPojo = new LoginRequestPOJO("iamfd", "password");
		} else if (role.equalsIgnoreCase("sup")) {
			loginPojo = new LoginRequestPOJO("iamsup", "password");

		} else if (role.equalsIgnoreCase("eng")) {
			loginPojo = new LoginRequestPOJO("iameng", "password");

		} else if (role.equalsIgnoreCase("qc")) {
			loginPojo = new LoginRequestPOJO("iamqc", "password");

		}

		String token = given().when().header(new Header("Content-type", "application/json")).and()
				.body(getJsonObject(loginPojo)).and().log().all().post("/login").then().extract().jsonPath()
				.getString("data.token");

		return token;
	}

	public static String getTime() {
		Date date = new Date();
		System.out.println(date.toString());

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY HH-mm");
		String formatedDate = sdf.format(date);

		return formatedDate;
	}

	public static com.ui.pojo.CreateJobPOJO createFakeData() {

		Faker faker = new Faker();
		com.ui.pojo.CreateJobPOJO data = new com.ui.pojo.CreateJobPOJO(IOem.GOOGLE, "Nexus", faker.name().firstName(),
				faker.name().lastName(), faker.internet().emailAddress(), faker.numerify("98########"));

		return data;
	}

	public static boolean searchEntryInList(List<DashboardTablePOJO> dataList, DashboardTablePOJO data) {
		Iterator<DashboardTablePOJO> dataIterator = dataList.iterator();
		boolean status = false;
		while (dataIterator.hasNext()) {
			DashboardTablePOJO dataFromTable = dataIterator.next();
			System.out.println(dataFromTable);
			System.out.println(data);
			System.out.println("---------------------------------");
			if (dataFromTable.equals(data)) {
				status = true;
				break;
			} else {
				status = false;
			}
		}
		return status;
	}
//SMS ---- Twilio APIS
	public static String readEmailAndGetOTP() {
		final String username = "testautomationacademy33@gmail.com";
		final String password = "nijq rnsf kkut gavy"; // hxbs kirl ioxg erog
		String OTPcode = null;
		Properties prop = new Properties();
		prop.put("mail.store.protocol", "imaps"); // Changed protocol to "imaps"
		prop.put("mail.imaps.host", "imap.gmail.com");
		prop.put("mail.imaps.port", "993");
		prop.put("mail.imaps.auth", "true");

		Session session = Session.getInstance(prop, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {
			// Connect to the Gmail server
			Store store = session.getStore("imaps"); // Changed to "imaps"
			store.connect("imap.gmail.com", username, password);

			// Get the inbox folder
			Folder inbox = store.getFolder("INBOX");
			inbox.open(Folder.READ_ONLY);

			// Get messages in the inbox
			Message[] messages = inbox.getMessages();

			// Display information about each message
			Message message = messages[messages.length - 1]; // 0 n-1
			System.out.println("Subject: " + message.getSubject());
			System.out.println("From: " + message.getFrom()[0]);
			System.out.println("Sent Date: " + message.getSentDate());
			System.out.println("Received Date: " + message.getReceivedDate());
			System.out.println("------------------------------------");
			Object content = message.getContent();

			if (content instanceof MimeMultipart) {
				String data = handleMultipart((MimeMultipart) content);
				OTPcode = getOTP(data);
			} else {
				System.out.println("Content: " + content);
			}

			System.out.println("------------------------------------");

			// Close the connection
			inbox.close(false);
			store.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return OTPcode;
	}

	public static boolean searchEntryInList(List<DashboardTablePOJO> dataList, String imeiNumber) {
		ImeiFilter imeiFilter = new ImeiFilter(imeiNumber);
		Object[] data = dataList.stream().filter(imeiFilter).toArray();
		if (data.length >= 1) {
			System.out.println("found");
			return true;
		} else {
			return false;

		}
	}

	private static String handleMultipart(MimeMultipart multipart) throws MessagingException, IOException {
		String messagebody = null;

		BodyPart bodyPart = multipart.getBodyPart(0);
		Object partContent = bodyPart.getContent();

		if (partContent instanceof String) {
			System.out.println("Part Content: " + partContent);
			messagebody = partContent + "";
		}

		return messagebody;
	}

	private static String getOTP(String data) {
		System.out.println(data.substring(12, 17));
		return data.substring(12, 17);
	}

}
